import { container } from "../../../nextjs-material-kit";

const carouselStyle = {
  section: {
    padding: "0"
  },
  container,
  marginAuto: {
    width: "100%",
  }
};

export default carouselStyle;
