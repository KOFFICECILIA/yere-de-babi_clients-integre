import React from "react";
// react component for creating beautiful carousel
import Carousel from "react-slick";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import LocationOn from "@material-ui/icons/LocationOn";
// core components
import GridContainer from "../GridContainer";
import GridItem from "../GridItem";
import Card from "../Card";
;

import styles from "../../public/jss/nextjs-material-kit/pages/componentsSections/carouselStyle";

const useStyles = makeStyles(styles);

export default function TestimoCarousel() {
  const classes = useStyles();
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  };
  return (
    <div className={classes.section}>
      <div className={classes.container}>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12} className={classes.marginAuto} style={{width:"100%"}}>
            <Card carousel>
              <Carousel {...settings} className="utf_testimonial_carousel" style={{height:"450px",width:"100%" }}>
                <div className="">
                    <div className="">
                        <div className="utf_carousel_review_part">
                            <div className="utf_testimonial_box">
                                <div className="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</div>
                            </div>
                            <div className="utf_testimonial_author"> <img src="../images/user2.jpg" alt="" className="img-fluid" />
                                <h4>Denwen Evil <span>Web Developer</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="">
                    <div className="utf_carousel_review_part">
                        <div className="utf_testimonial_box">
                            <div className="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</div>
                        </div>
                        <div className="utf_testimonial_author"> 
                            <img src="../images/user3.jpg" alt="" />
                            <h4>Adam Alloriam <span>Web Developer</span></h4>
                        </div>
                    </div>
                </div>
                <div className="">
                    <div className="utf_carousel_review_part">
                        <div className="utf_testimonial_box">
                            <div className="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</div>
                        </div>
                        <div className="utf_testimonial_author"> <img src="images/user4.jpg" alt="" />
                            <h4>Illa Millia <span>Project Manager</span></h4>
                        </div>
                    </div>
                </div>
                <div className="">
                    <div className="utf_carousel_review_part">
                        <div className="utf_testimonial_box">
                            <div className="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</div>
                        </div>
                        <div className="utf_testimonial_author"> <img src="images/user01.jpg" alt="" />
                            <h4>Denwen Evil <span>Web Developer</span></h4>
                        </div>
                    </div>
                </div>
              </Carousel>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
