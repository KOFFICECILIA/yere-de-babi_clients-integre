import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';


const useStyles = makeStyles({
  list: {
   
  },
  fullList: {
    
  },
});

export default function MobileDrawer() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
       
          <ListItem style={{display:'block', textAlign:'center'}}>
            <ListItemText>
            <a href="/" style={{fontSize:'20px',}}>Accueil</a>
            </ListItemText>
            <ListItemText>
            <a href="/Liste_annonces_list" style={{fontSize:'20px',}}>Maps</a>
            </ListItemText>
            <ListItemText>
            <a href="/About" style={{fontSize:'20px',}}>A Propos de Nous</a>
            </ListItemText>
            <ListItemText>
            <a href="/Contact" style={{fontSize:'20px',}}>Contact</a>
            </ListItemText>
          </ListItem>
      </List>
      
    </div>
  );

  return (
    <div>
      {['Top'].map((anchor) => (
        <React.Fragment key={anchor}>
          <Button className="utfbutton_collapse" onClick={toggleDrawer(anchor, true)}>{anchor}</Button>
          <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
