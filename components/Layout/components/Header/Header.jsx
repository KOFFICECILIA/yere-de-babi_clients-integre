import React from 'react'
import FormDialog from './Forms/login_form'
import MobileDrawer from './MobileDrawer/MobileDrawer'

export default function Header() {
    return(
        <header id="header_part" class='' style={{backgroundColor:'white'}}> 
            <div id="header">
            <div className="container"> 
                <div className="utf_left_side"> 
                <div id="logo"> <a href="/"><img src="images/yèrè2babi.png" alt="" /></a> </div>
                
                <div className="mmenu-trigger">
                    {/* <button className="hamburger utfbutton_collapse" type="button">
                        <span className="utf_inner_button_box">
                            <span className="utf_inner_section"></span>
                        </span>
                    </button> */}
                    <MobileDrawer/>
                </div>

                <nav id="navigation" className="style_one">
                    <ul id="responsive">
                    <li><a href="/">Accueil</a></li>			  
                    <li><a href="/Liste_annonces_list">Maps</a></li>
                    <li><a href="/About">A Propos de Nous</a></li>
                    <li><a href="/Contact">Contact</a></li>              
                    </ul>
                </nav>
                <div className="clearfix"></div>
                </div>
                <div className="utf_right_side">
                <div className="header_widget"><FormDialog/></div>
                </div>
                
                
                <div id="dialog_signin_part" className="zoom-anim-dialog mfp-hide">
                <div className="small_dialog_header">
                    <h3>S'identifier</h3>
                </div>
                <div className="utf_signin_form style_one">
                    <ul className="utf_tabs_nav">
                    <li className=""><a href="#tab1">Connexion</a></li>
                    <li><a href="#tab2">Inscription</a></li>
                    </ul>
                    <div className="tab_container alt"> 
                    <div className="tab_content" id="tab1" style={{display:"none"}}>
                        <form method="post" className="login">
                        <a href="javascript:void(0);" className="social_bt facebook_btn"><i className="fa fa-facebook"></i>Connexion avec Facebook</a>
                        <a href="javascript:void(0);" className="social_bt google_btn"><i className="fa fa-google-plus"></i>Connexion avec Google</a>	
                        <p className="utf_row_form utf_form_wide_block">
                            <label for="username">
                            <input type="text" className="input-text" name="username" id="username" value="" placeholder="Username" />
                            </label>
                        </p>
                        <p className="utf_row_form utf_form_wide_block">
                            <label for="password">
                            <input className="input-text" type="password" name="password" id="password" placeholder="Password"/>
                            </label>
                        </p>
                        <div className="utf_row_form utf_form_wide_block form_forgot_part"> <span className="lost_password fl_left"> <a href="javascript:void(0);">Forgot Password?</a> </span>
                            <div className="checkboxes fl_right">
                            <input id="remember-me" type="checkbox" name="check" />
                            <label for="remember-me">Remember Me</label>
                            </div>
                        </div>
                        <div className="utf_row_form">
                            <input type="submit" className="button border margin-top-5" name="login" value="Login" />
                        </div>
                        </form>
                    </div>
                    
                    <div className="tab_content" id="tab2" style={{display:"none"}}>
                        <form method="post" className="register">
                        <p className="utf_row_form utf_form_wide_block">
                            <label for="username2">
                            <input type="text" className="input-text" name="username" id="username2" value="" placeholder="Username" />
                            </label>
                        </p>
                        <p className="utf_row_form utf_form_wide_block">
                            <label for="email2">
                            <input type="text" className="input-text" name="email" id="email2" value="" placeholder="Email" />
                            </label>
                        </p>
                        <p className="utf_row_form utf_form_wide_block">
                            <label for="password1">
                            <input className="input-text" type="password" name="password1" id="password1" placeholder="Password" />
                            </label>
                        </p>
                        <p className="utf_row_form utf_form_wide_block">
                            <label for="password2">
                            <input className="input-text" type="password" name="password2" id="password2" placeholder="Confirm Password" />
                            </label>
                        </p>
                        <input type="submit" className="button border fw margin-top-10" name="register" value="Register" />
                        </form>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>    
        </header>
    )
}