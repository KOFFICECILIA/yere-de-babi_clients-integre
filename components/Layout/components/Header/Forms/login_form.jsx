import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import TabPanel from '../Forms/TabsForm'

export default function FormDialog() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen} className="button border sign-in popup-with-zoom-anim">
        <ExitToAppIcon fontSize="large"/> S'identifier
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          <div className="small_dialog_header">
            <h3>S'identifier</h3>
          </div>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <div className="utf_signin_form style_one">
              <TabPanel/>
            </div>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}