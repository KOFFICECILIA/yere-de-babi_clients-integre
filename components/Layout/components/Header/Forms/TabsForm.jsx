import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example" className="utf_tabs_nav">
          <Tab label="Connexion" {...a11yProps(0)} className="PrivateTabIndicator-colorSecondary ourTab" />
          <Tab label="Inscription" {...a11yProps(1)} className="ourTab" />
        </Tabs>
      </AppBar>
        <TabPanel value={value} index={0}>
            <form method="post" className="login">
                <a href="javascript:void(0);" className="social_bt facebook_btn"><i className="fa fa-facebook"></i>Connexion avec Facebook</a>
                <a href="javascript:void(0);" className="social_bt google_btn"><i className="fa fa-google-plus"></i>Connexion avec Google</a>	
                <p className="utf_row_form utf_form_wide_block">
                    <label for="username">
                    <input type="text" className="input-text" name="username" id="username" value="" placeholder="Username" />
                    </label>
                </p>
                <p className="utf_row_form utf_form_wide_block">
                    <label for="password">
                    <input className="input-text" type="password" name="password" id="password" placeholder="Password"/>
                    </label>
                </p>
                <div className="utf_row_form utf_form_wide_block form_forgot_part"> <span className="lost_password fl_left"> <a href="javascript:void(0);">Forgot Password?</a> </span>
                    <div className="checkboxes fl_right">
                    <input id="remember-me" type="checkbox" name="check" />
                    <label for="remember-me">Remember Me</label>
                    </div>
                </div>
                <div className="utf_row_form">
                    <input type="submit" className="button border margin-top-5" name="login" value="Login" />
                </div>
            </form>
        </TabPanel>
        <TabPanel value={value} index={1}>
            <form method="post" className="register">
            <a href="javascript:void(0);" className="social_bt facebook_btn"><i className="fa fa-facebook"></i>Connexion avec Facebook</a>
                <a href="javascript:void(0);" className="social_bt google_btn"><i className="fa fa-google-plus"></i>Connexion avec Google</a>	
                <p className="utf_row_form utf_form_wide_block">
                    <label for="username2">
                    <input type="text" className="input-text" name="username" id="username2" value="" placeholder="Username" />
                    </label>
                </p>
                <p className="utf_row_form utf_form_wide_block">
                    <label for="email2">
                    <input type="text" className="input-text" name="email" id="email2" value="" placeholder="Email" />
                    </label>
                </p>
                <p className="utf_row_form utf_form_wide_block">
                    <label for="password1">
                    <input className="input-text" type="password" name="password1" id="password1" placeholder="Password" />
                    </label>
                </p>
                <p className="utf_row_form utf_form_wide_block">
                    <label for="password2">
                    <input className="input-text" type="password" name="password2" id="password2" placeholder="Confirm Password" />
                    </label>
                </p>
                <input type="submit" className="button border fw margin-top-10" name="register" value="Register" />
            </form>
        </TabPanel>
    </div>
  );
}