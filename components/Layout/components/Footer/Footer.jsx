import React from 'react'

export default function Footer(){
    return(
        <div>
            <div id="footer" className="footer_sticky_part"> 
                <div className="container">
                <div className="row">
                    <div className="col-md-12">
                    <div className="footer_copyright_part">Copyright © WeenovIt 2021 All Rights Reserved.</div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    )
}