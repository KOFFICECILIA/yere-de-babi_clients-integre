import React from "react";
// react component for creating beautiful carousel
import Carousel from "react-slick";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import LocationOn from "@material-ui/icons/LocationOn";
// core components
import GridContainer from "../GridContainer";
import GridItem from "../GridItem";
import Card from "../Card";
;

import styles from "../../public/jss/nextjs-material-kit/pages/componentsSections/carouselStyle";

const useStyles = makeStyles(styles);

export default function DetailsCarousel() {
  const classes = useStyles();
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  };
  return (
    <div className={classes.section}>
      <div className={classes.containerFluid}>
        <GridContainer className={{padding:"0"}}>
          <GridItem xs={12} sm={12} md={12} lg={12} className={classes.marginAuto}>
            <Card carousel>
              <Carousel {...settings} className="utf_listing_slider utf_gallery_container margin-bottom-0">
                    <a href="images/events.jpg"  className="item utf_gallery"><img src="images/events.jpg" alt="" style={{height:"100%"}}/></a> 
                    <a href="images/hotels.jpg" data-background-image="" className="item utf_gallery"><img src="images/hotels.jpg" alt="" style={{height:"100%"}}/></a> 
                    <a href="images/beach.jpg" data-background-image="" className="item utf_gallery"><img src="images/beach.jpg" alt="" style={{height:"100%"}}/></a> 
                    <a href="images/nightc.jpg" data-background-image="" className="item utf_gallery"><img src="images/nightc.jpg" alt="" style={{height:"100%"}}/></a> 
              </Carousel>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
