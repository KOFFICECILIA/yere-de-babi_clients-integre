import React from "react";
// react component for creating beautiful carousel
import Carousel from "react-slick";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import LocationOn from "@material-ui/icons/LocationOn";
// core components
import GridContainer from "../GridContainer";
import GridItem from "../GridItem";
import Card from "../Card";
;

import styles from "../../public/jss/nextjs-material-kit/pages/componentsSections/carouselStyle";

const useStyles = makeStyles(styles);

export default function PaternCarousel() {
  const classes = useStyles();
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  };
  return (
    <div className={classes.section}>
      <div className={classes.containerfluid}>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12} className={classes.marginAuto}>
            <Card carousel style={{margin:"auto"}}>
              <Carousel {...settings} className="companie-logo-slick-carousel" style={{maxHeight:"360px"}}>
                <div className="col-md-12 p-0" style={{margin:"10px"}} xs={12} sm={12} md={12}>
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div className="col-md-12 p-0" style={{margin:"40px"}} xs={12} sm={12} md={12}>
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 p-0" style={{margin:"10px"}} xs={12} sm={12} md={12}>
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                            <div className="col-xs-4 col-sm-4 col-md-2 col-lg-2 p-1">
                                <div className="item">
                                    <img src="images/brand_logo_01.png" style={{minHeight:"100%"}} alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </Carousel>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
