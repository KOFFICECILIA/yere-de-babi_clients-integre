import React from "react";
// react component for creating beautiful carousel
import Carousel from "react-slick";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import LocationOn from "@material-ui/icons/LocationOn";
// core components
import GridContainer from "../GridContainer";
import GridItem from "../GridItem";
import Card from "../Card";
;

import styles from "../../public/jss/nextjs-material-kit/pages/componentsSections/carouselStyle";

const useStyles = makeStyles(styles);

export default function AnnoncesCarousel() {
  const classes = useStyles();
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
  };
  return (
    <div className={classes.section}>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12} lg={12} className={classes.marginAuto}>
            <Card carousel style={{margin:"auto"}}>
              <Carousel {...settings}className="utf_listing_slider slideT" style={{width:"80vw", backgroundColor:'transparent'}}>
                <div>
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-6">
                            <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                <div className="utf_listing_item"> <img src="images/restauration.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurant</span> <span className="featured_tag">Featured</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Chontaduro Barcelona</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>					  
                                </div>
                                <div className="utf_star_rating_section" data-rating="4.5">
                                    <div className="utf_counter_star_rating">(4.5)</div>
                                    <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                    <span className="like-icon"></span>
                                </div>
                                </a> 
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-6">
                            <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                <div className="utf_listing_item"> <img src="images/restauration.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurant</span> <span className="featured_tag">Featured</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Chontaduro Barcelona</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>					  
                                </div>
                                <div className="utf_star_rating_section" data-rating="4.5">
                                    <div className="utf_counter_star_rating">(4.5)</div>
                                    <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                    <span className="like-icon"></span>
                                </div>
                                </a> 
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-6">
                            <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                <div className="utf_listing_item"> <img src="images/restauration.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurant</span> <span className="featured_tag">Featured</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Chontaduro Barcelona</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>					  
                                </div>
                                <div className="utf_star_rating_section" data-rating="4.5">
                                    <div className="utf_counter_star_rating">(4.5)</div>
                                    <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                    <span className="like-icon"></span>
                                </div>
                                </a> 
                            </div>
                        </div>
                    
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-6">
                            <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                <div className="utf_listing_item"> <img src="images/restauration.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurant</span> <span className="featured_tag">Featured</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Chontaduro Barcelona</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>					  
                                </div>
                                <div className="utf_star_rating_section" data-rating="4.5">
                                    <div className="utf_counter_star_rating">(4.5)</div>
                                    <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                    <span className="like-icon"></span>
                                </div>
                                </a> 
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-6">
                            <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                <div className="utf_listing_item"> <img src="images/restauration.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurant</span> <span className="featured_tag">Featured</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Chontaduro Barcelona</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>					  
                                </div>
                                <div className="utf_star_rating_section" data-rating="4.5">
                                    <div className="utf_counter_star_rating">(4.5)</div>
                                    <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                    <span className="like-icon"></span>
                                </div>
                                </a> 
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-6">
                            <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                <div className="utf_listing_item"> <img src="images/restauration.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurant</span> <span className="featured_tag">Featured</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Chontaduro Barcelona</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>					  
                                </div>
                                <div className="utf_star_rating_section" data-rating="4.5">
                                    <div className="utf_counter_star_rating">(4.5)</div>
                                    <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                    <span className="like-icon"></span>
                                </div>
                                </a> 
                            </div>
                        </div>
                    
                    </div>


                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-6">
                            <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                <div className="utf_listing_item"> <img src="images/restauration.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurant</span> <span className="featured_tag">Featured</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Chontaduro Barcelona</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>					  
                                </div>
                                <div className="utf_star_rating_section" data-rating="4.5">
                                    <div className="utf_counter_star_rating">(4.5)</div>
                                    <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                    <span className="like-icon"></span>
                                </div>
                                </a> 
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-6">
                            <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                <div className="utf_listing_item"> <img src="images/restauration.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurant</span> <span className="featured_tag">Featured</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Chontaduro Barcelona</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>					  
                                </div>
                                <div className="utf_star_rating_section" data-rating="4.5">
                                    <div className="utf_counter_star_rating">(4.5)</div>
                                    <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                    <span className="like-icon"></span>
                                </div>
                                </a> 
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-6">
                            <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                <div className="utf_listing_item"> <img src="images/restauration.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurant</span> <span className="featured_tag">Featured</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Chontaduro Barcelona</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>					  
                                </div>
                                <div className="utf_star_rating_section" data-rating="4.5">
                                    <div className="utf_counter_star_rating">(4.5)</div>
                                    <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                    <span className="like-icon"></span>
                                </div>
                                </a> 
                            </div>
                        </div>
                    
                    </div>


                </div>
                {/* <div className="">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-6">
                                <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                    <div className="utf_listing_item"> <img src="images/restauration.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurant</span> <span className="featured_tag">Featured</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Chontaduro Barcelona</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>					  
                                    </div>
                                    <div className="utf_star_rating_section" data-rating="4.5">
                                        <div className="utf_counter_star_rating">(4.5)</div>
                                        <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                        <span className="like-icon"></span>
                                    </div>
                                    </a> 
                                </div>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-6">
                                <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                    <div className="utf_listing_item"> <img src="images/events.jpg" alt="" /> <span className="tag"><i className="im im-icon-Electric-Guitar"></i> Evènement</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>							
                                        </div>
                                        <h3>The Lounge & Bar</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>												
                                    </div>
                                    </div>
                                    <div className="utf_star_rating_section" data-rating="4.5">
                                        <div className="utf_counter_star_rating">(4.5)</div>
                                        <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                        <span className="like-icon"></span>
                                    </div>
                                </a> 
                            </div>
                            </div>
                            <div className="col-4 mb-6">
                            <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                <div className="utf_listing_item"> <img src="images/hotels.jpg" alt="" /> <span className="tag"><i className="im im-icon-Hotel"></i> Hôtels</span>
                                <span className="utf_closed">Closed</span>
                                <div className="utf_listing_item_content">
                                    <div className="utf_listing_prige_block">							
                                        <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>							
                                    </div>
                                    <h3>Westfield Sydney</h3>
                                    <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                    <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>												
                                </div>
                                </div>
                                <div className="utf_star_rating_section" data-rating="4.5">
                                    <div className="utf_counter_star_rating">(4.5)</div>
                                    <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                    <span className="like-icon"></span>
                                </div>
                                </a> 
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="">
                    <div className="container">
                        <div className="row">
                            <div className="col-4 mb-6">
                                <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                    <div className="utf_listing_item"> <img src="images/utf_listing_item-04.jpg" alt="" /> <span className="tag"><i className="im im-icon-Dumbbell"></i> Fitness</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Ruby Beauty Center</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>												
                                    </div>
                                    </div>
                                    <div className="utf_star_rating_section" data-rating="4.5">
                                        <div className="utf_counter_star_rating">(4.5)</div>
                                        <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                        <span className="like-icon"></span>
                                    </div>
                                    </a> 
                                </div>
                            </div>
                            <div className="col-4 mb-6">
                                <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                    <div className="utf_listing_item"> 
                                        <img src="images/utf_listing_item-05.jpg" alt="" /> 
                                        <span className="tag"><i className="im im-icon-Hotel"></i> Hôtels</span> 
                                        <span className="featured_tag">Featured</span>
                                    <span className="utf_closed">Closed</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>							
                                        </div>
                                        <h3>UK Fitness Club</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>												
                                    </div>
                                    </div>
                                    <div className="utf_star_rating_section" data-rating="4.5">
                                        <div className="utf_counter_star_rating">(4.5)</div>
                                        <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                        <span className="like-icon"></span>
                                    </div>
                                    </a> 
                                </div>
                            </div>
                            <div className="col-4 mb-6">
                                <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                    <div className="utf_listing_item"> <img src="images/utf_listing_item-06.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurants</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $45</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Fairmont Pacific Rim</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>
                                    </div>
                                    <div className="utf_star_rating_section" data-rating="4.5">
                                        <div className="utf_counter_star_rating">(4.5)</div>
                                        <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                        <span className="like-icon"></span>
                                    </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="">
                    <div className="container">
                        <div className="row">
                            <div className="col-4 mb-6">
                                <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                    <div className="utf_listing_item"> <img src="images/utf_listing_item-04.jpg" alt="" /> <span className="tag"><i className="im im-icon-Dumbbell"></i> Fitness</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Ruby Beauty Center</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>												
                                    </div>
                                    </div>
                                    <div className="utf_star_rating_section" data-rating="4.5">
                                        <div className="utf_counter_star_rating">(4.5)</div>
                                        <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                        <span className="like-icon"></span>
                                    </div>
                                    </a> 
                                </div>
                            </div>
                            <div className="col-4 mb-6">
                                <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                    <div className="utf_listing_item"> 
                                        <img src="images/utf_listing_item-05.jpg" alt="" /> 
                                        <span className="tag"><i className="im im-icon-Hotel"></i> Hôtels</span> 
                                        <span className="featured_tag">Featured</span>
                                    <span className="utf_closed">Closed</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>							
                                        </div>
                                        <h3>UK Fitness Club</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>												
                                    </div>
                                    </div>
                                    <div className="utf_star_rating_section" data-rating="4.5">
                                        <div className="utf_counter_star_rating">(4.5)</div>
                                        <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                        <span className="like-icon"></span>
                                    </div>
                                    </a> 
                                </div>
                            </div>
                            <div className="col-4 mb-6">
                                <div className="utf_carousel_item"> <a href="/Detail_annonces" className="utf_listing_item-container compact">
                                    <div className="utf_listing_item"> <img src="images/utf_listing_item-06.jpg" alt="" /> <span className="tag"><i className="im im-icon-Chef-Hat"></i> Restaurants</span>
                                    <span className="utf_open_now">Open Now</span>
                                    <div className="utf_listing_item_content">
                                        <div className="utf_listing_prige_block">							
                                            <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $45</span>							
                                            <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                                        </div>
                                        <h3>Fairmont Pacific Rim</h3>
                                        <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
                                        <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>											
                                    </div>
                                    </div>
                                    <div className="utf_star_rating_section" data-rating="4.5">
                                        <div className="utf_counter_star_rating">(4.5)</div>
                                        <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
                                        <span className="like-icon"></span>
                                    </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> */}
             
              </Carousel>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
