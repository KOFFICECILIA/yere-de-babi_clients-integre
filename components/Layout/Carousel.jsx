import React from "react";
// react component for creating beautiful carousel
import Carousel from "react-slick";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import LocationOn from "@material-ui/icons/LocationOn";
// core components
import GridContainer from "../GridContainer";
import GridItem from "../GridItem";
import Card from "../Card";
;

import styles from "../../public/jss/nextjs-material-kit/pages/componentsSections/carouselStyle";

const useStyles = makeStyles(styles);

export default function SectionCarousel() {
  const classes = useStyles();
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  };
  return (
    <div className={classes.section}>
      <div className={classes.containerFluid}>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12} lg={12} className={classes.marginAuto}>
            <Card carousel style={{margin:"0", borderRadius:"0"}}>
              <Carousel {...settings} className="rev_slider_wrapper utf_listing_slider">
                <div id="utf_rev_slider_block" className="rev_slider home fullwidthabanner">
                  <img src="../images/sider1.jpg" alt="First slide" className="slick-image rev-slidebg" style={{minHeight:'85vh'}}/>
                    <div className="slick-caption utf_custom_caption">
                        <div>
                        <div className="slick-text_desc">
                            <div style={{marginBottom:'20px'}}>
                                <div className="utf_item_title margin-bottom-15">
                                    <h1 style={{zIndex:"6",color:"#fff",letterSpacing:"0px",fontWeight:"600",fontSize:'56px'}}>Trouver les meilleures Attractions</h1>
                                </div>
                                <div class="utf_rev_description_text">Propriétaires d'entreprises - plus d'un million de personnes consultent ces listes chaque <br/> mois - nous vous recommandons vivement de vous assurer que votre entreprise est <br/> correctement répertoriée.</div>
                                <br/> 
                            </div>
                        </div>
                       <div className="container main_inner_search_block">
                        <div className="main_input_search_part">
                            <div className="main_input_search_part_item">
                                <input type="text" placeholder="Que rechercher-vous ?" value="" />
                            </div>
                            <div className="main_input_search_part_item intro-search-field">
                                <select className="utf_chosen_select_single" dataLiveSearch="true" dataSelectedTextFormat="count" dataSize="7" title="Sélectionnez le lieu">
                                <option>Afghanistan</option>
                                <option>Albania</option>
                                <option>Algeria</option>
                                <option>Brazil</option>
                                <option>Burundi</option>
                                <option>Bulgaria</option>
                                <option>Germany</option>
                                <option>Grenada</option>
                                <option>Guatemala</option>
                                <option>Iceland</option>
                                </select>
                            </div>
                            <div className="main_input_search_part_item intro-search-field">
                                <select dataPlaceholder="Toutes Categories" className="utf_chosen_select_single" title="Toutes Categories" dataLiveSearch="true" dataSelectedTextFormat="count" dataSize="7">
                                <option>Food & Restaurants </option>
                                <option>Shop & Education</option>
                                <option>Education</option>
                                <option>Business</option>
                                <option>Events</option>
                                </select>
                            </div>
                            <button className="button">Recherche</button>
                        </div>
                       </div>
                        </div>
                    </div>
                </div>
                <div className="rev_slider home fullwidthabanner">
                    <img src={"../images/slider2.jpg"} alt="Second slide" className="slick-image rev-slidebg" style={{minHeight:'85vh'}} />
                    <div className="slick-caption utf_custom_caption">
                       <div>
                       <div className="slick-text_desc">
                            <div className="">
                                <div className="utf_item_title margin-bottom-15">
                                    <h1 style={{zIndex:"6",color:"#fff",letterSpacing:"0px",fontWeight:"600",fontSize:'56px'}}>Trouver les meilleures Attractions </h1>
                                </div>
                                <div class="utf_rev_description_text">Propriétaires d'entreprises - plus d'un million de personnes consultent ces listes chaque <br/> mois - nous vous recommandons vivement de vous assurer que votre entreprise est <br/> correctement répertoriée.</div>
                                <br/> 
                            </div>
                        </div>
                       <div className="container main_inner_search_block">
                        <div className="main_input_search_part">
                            <div className="main_input_search_part_item">
                                <input type="text" placeholder="Que rechercher-vous ?" value="" />
                            </div>
                            <div className="main_input_search_part_item intro-search-field">
                                <select className="utf_chosen_select_single" dataLiveSearch="true" dataSelectedTextFormat="count" dataSize="7" title="Sélectionnez le lieu">
                                <option>Afghanistan</option>
                                <option>Albania</option>
                                <option>Algeria</option>
                                <option>Brazil</option>
                                <option>Burundi</option>
                                <option>Bulgaria</option>
                                <option>Germany</option>
                                <option>Grenada</option>
                                <option>Guatemala</option>
                                <option>Iceland</option>
                                </select>
                            </div>
                            <div className="main_input_search_part_item intro-search-field">
                                <select dataPlaceholder="Toutes Categories" className="utf_chosen_select_single" title="Toutes Categories" dataLiveSearch="true" dataSelectedTextFormat="count" dataSize="7">
                                <option>Food & Restaurants </option>
                                <option>Shop & Education</option>
                                <option>Education</option>
                                <option>Business</option>
                                <option>Events</option>
                                </select>
                            </div>
                            <button className="button">Recherche</button>
                        </div>
                       </div>
                       </div>
                    </div>
                </div>
                <div  className="rev_slider home fullwidthabanner">
                  <img src={"../images/slider3.jpg"} alt="Third slide" className="slick-image rev-slidebg" style={{minHeight:'85vh'}} />
                    <div className="slick-caption utf_custom_caption">
                        <div>
                        <div className="slick-text_desc">
                            <div className="">
                                <div className="utf_item_title margin-bottom-15">
                                    <h1 style={{zIndex:"6",color:"#fff",letterSpacing:"0px",fontWeight:"600",fontSize:'56px'}}>Trouver les meilleures Attractions </h1>
                                </div>
                                <div class="utf_rev_description_text">Propriétaires d'entreprises - plus d'un million de personnes consultent ces listes chaque <br/> mois - nous vous recommandons vivement de vous assurer que votre entreprise est <br/> correctement répertoriée.</div>
                                <br/> 
                            </div>
                        </div>
                       <div className="container main_inner_search_block">
                        <div className="main_input_search_part">
                            <div className="main_input_search_part_item">
                                <input type="text" placeholder="Que rechercher-vous ?" value="" />
                            </div>
                            <div className="main_input_search_part_item intro-search-field">
                                <select className="utf_chosen_select_single" dataLiveSearch="true" dataSelectedTextFormat="count" dataSize="7" title="Sélectionnez le lieu">
                                <option>Afghanistan</option>
                                <option>Albania</option>
                                <option>Algeria</option>
                                <option>Brazil</option>
                                <option>Burundi</option>
                                <option>Bulgaria</option>
                                <option>Germany</option>
                                <option>Grenada</option>
                                <option>Guatemala</option>
                                <option>Iceland</option>
                                </select>
                            </div>
                            <div className="main_input_search_part_item intro-search-field">
                                <select dataPlaceholder="Toutes Categories" className="utf_chosen_select_single" title="Toutes Categories" dataLiveSearch="true" dataSelectedTextFormat="count" dataSize="7">
                                <option>Food & Restaurants </option>
                                <option>Shop & Education</option>
                                <option>Education</option>
                                <option>Business</option>
                                <option>Events</option>
                                </select>
                            </div>
                            <button className="button">Recherche</button>
                        </div>
                       </div>
                        </div>
                    </div>
                </div>
              </Carousel>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
