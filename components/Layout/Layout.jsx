import React from 'react'
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'

export default function Layout({children}) {
    return(
        <>
            <div id="main_wrapper">
                <Header />
                {children}
                <Footer/>
                <div id="bottom_backto_top"><a href="#"></a></div>
            </div>
        </>
    )
}