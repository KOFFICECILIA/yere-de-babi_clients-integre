import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Popper from '@material-ui/core/Popper';
import PopupState, { bindToggle, bindPopper } from 'material-ui-popup-state';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  typography: {
    padding: theme.spacing(2),
  },
}));

export default function ServicesOne() {
  const classes = useStyles();

  return (
    <PopupState variant="popper" popupId="demo-popup-popper">
      {(popupState) => (
        <div style={{marginTop:'23px'}}>
          <p variant="contained"  className="navbar-nav-link dropdown-toggle my-drop" {...bindToggle(popupState)}>
          <h3>Remplissez  <br/>le formulaire  <br/>ci-dessous</h3>
          </p>
          <Popper {...bindPopper(popupState)} style={{width:'auto'}} transition>
            {({ TransitionProps }) => (
              <Fade {...TransitionProps} timeout={350}>
                <Paper>
                  <div className="container-fluid">
                    <div className="col-4">
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                     Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar.
                      Donec a consectetur nulla. Nulla posuere sapien vitae
                       lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat.
                    </p>
                    </div>
                  </div>
                </Paper>
              </Fade>
            )}
          </Popper>
        </div>
      )}
    </PopupState>
  );
}

