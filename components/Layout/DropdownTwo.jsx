import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';


export default function OurDropdownTwo(){
    const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

return(
    <div className="panel-dropdown">
        <Button 
      aria-controls="simple-menu" 
      aria-haspopup="true" 
      onClick={handleClick} 
      style={{border: "none",
        cursor: "pointer",
        borderRadius: "5px",
        boxShadow: "0 1px 6px 0 rgb(0 0 0 / 10%)",
        fontSize: "13px",
        fontWeight:"400" ,
        height: "auto",
        padding: "10px 16px",
        lineHeight: "30px",
        margin: "0 0 15px",
        position: "relative",
        backgroundColor: "#fff",
        textAlign: "left",
        color: "#888",
        display: "block",
        width: "100%",
        transition: "color .3s",
    justifyContent:"space-between"}}
        >
        Guests <span className="qtyTotal" name="qtyTotal">1</span> <i class="fa fa-angle-double-down"></i>
      </Button>
      <Menu
      className="panel-dropdown-content padding-reset"
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        style={{width:"400px", height:"350px"}}
      >
        <MenuItem onClick={handleClose} className="ovnone">
            <div className="panel-dropdown-content">
                <div className="qtyButtons">
                    <div className="qtyTitle">Adults</div>
                    <div className="qtyDec"></div>
                    <input type="text" name="qtyInput" value="1" />
                    <div className="qtyInc"></div>
                </div>
                <div className="qtyButtons">
                    <div className="qtyTitle">Childrens</div>
                    <div className="qtyDec"></div>
                    <input type="text" name="qtyInput" value="1" />
                    <div className="qtyInc"></div>
                </div>
            </div>
        </MenuItem>
      </Menu>
    </div>
)
}
