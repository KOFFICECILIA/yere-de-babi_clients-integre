import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

export default function SimpleMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className="panel-dropdown time-slots-dropdown">
      <Button 
      aria-controls="simple-menu" 
      aria-haspopup="true" 
      onClick={handleClick} 
      style={{border: "none",
        cursor: "pointer",
        borderRadius: "5px",
        boxShadow: "0 1px 6px 0 rgb(0 0 0 / 10%)",
        fontSize: "13px",
        fontWeight:"400" ,
        height: "auto",
        padding: "10px 16px",
        lineHeight: "30px",
        margin: "0 0 15px",
        position: "relative",
        backgroundColor: "#fff",
        textAlign: "left",
        color: "#888",
        display: "block",
        width: "100%",
        transition: "color .3s",
    justifyContent:"space-between"}}
        >
        Choose Time Slot... <i class="fa fa-angle-double-down"></i>
      </Button>
      <Menu
      className="panel-dropdown-content padding-reset"
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        style={{width:"400px", height:"350px"}}
      >
        <MenuItem onClick={handleClose}>
        <div className="time-slot">
            <input type="radio" name="time-slot" id="time-slot-1" />
            <label for="time-slot-1">
                <strong><span>1</span> : 8:00 AM - 8:30 AM</strong>
                <span>Choose 1 Table Available</span>
            </label>
        </div>
        </MenuItem>
        <MenuItem onClick={handleClose}>
        <div className="time-slot">
            <input type="radio" name="time-slot" id="time-slot-2" />
            <label for="time-slot-2">
                <strong><span>2</span> : 8:30 AM - 9:00 AM</strong>
                <span>Choose 1 Table Available</span>
            </label>
        </div>
        </MenuItem>
        <MenuItem onClick={handleClose}>
            <div className="time-slot">
                <input type="radio" name="time-slot" id="time-slot-3" />
                <label for="time-slot-3">
                    <strong><span>3</span> : 9:00 AM - 9:30 AM</strong>
                    <span>Choose 3 Table Available</span>
                </label>
            </div>
        </MenuItem>
        <MenuItem onClick={handleClose}>
            <div className="time-slot">
                <input type="radio" name="time-slot" id="time-slot-3" />
                <label for="time-slot-3">
                    <strong><span>3</span> : 9:00 AM - 9:30 AM</strong>
                    <span>Choose 3 Table Available</span>
                </label>
            </div>
        </MenuItem>
        <MenuItem onClick={handleClose}>
            <div className="time-slot">
                <input type="radio" name="time-slot" id="time-slot-3" />
                <label for="time-slot-3">
                    <strong><span>3</span> : 9:00 AM - 9:30 AM</strong>
                    <span>Choose 3 Table Available</span>
                </label>
            </div>
        </MenuItem>
      </Menu>
    </div>
  );
}
