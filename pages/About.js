import React from 'react'

export default function About() {
    return(
        <div>
              
  <div id="titlebar" className="gradient margin-bottom-0">
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h2>About Us</h2>          
          <nav id="breadcrumbs">
            <ul>
              <li><a href="/"> Accueil</a></li>
              <li>A Propos de Nous</li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  
  <section className="utf_testimonial_part fullwidth_block padding-top-75 padding-bottom-75"> 
    <div className="container">
      <div className="row">
        <div className="col-md-8 col-md-offset-2">
          <h3 className="headline_part centered">Avis de nos clients <span className="margin-top-15">Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since...</span> </h3>
        </div>
      </div>
    </div>
    <div className="fullwidth_carousel_container_block margin-top-20">
      <div className="utf_testimonial_carousel testimonials"> 
        <div className="utf_carousel_review_part">
          <div className="utf_testimonial_box">
            <div className="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</div>
          </div>
          <div className="utf_testimonial_author"> <img src="images/user2.jpg" alt="" />
            <h4>Denwen Evil <span>Web Developer</span></h4>
          </div>
        </div>
        <div className="utf_carousel_review_part">
          <div className="utf_testimonial_box">
            <div className="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</div>
          </div>
          <div className="utf_testimonial_author"> <img src="images/user3.jpg" alt="" />
            <h4>Adam Alloriam <span>Web Developer</span></h4>
          </div>
        </div>
        <div className="utf_carousel_review_part">
          <div className="utf_testimonial_box">
            <div className="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</div>
          </div>
          <div className="utf_testimonial_author"> <img src="images/user4.jpg" alt="" />
            <h4>Illa Millia <span>Project Manager</span></h4>
          </div>
        </div>
		    <div className="utf_carousel_review_part">
          <div className="utf_testimonial_box">
            <div className="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</div>
          </div>
          <div className="utf_testimonial_author"> <img src="images/user01.jpg" alt="" />
            <h4>Denwen Evil <span>Web Developer</span></h4>
          </div>
        </div>
      </div>
    </div>
  </section> 
  
  <div className="parallax bg-img" dataBackground="images/slider3.jpg"> 
    <div className="utf_text_content white-font">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 col-sm-12">
            <h2>Gérez et développez votre entreprise grâce au référencement<br/>Star de partout</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
            <a href="/Liste_annonces_list" className="button margin-top-25">Commencez</a> </div>
        </div>
      </div>
    </div>   
  </div>
	
  <section className="fullwidth_block padding-bottom-70" data-background-color="#f9f9f9"> 
	  <div className="container">
		<div className="row">
		  <div className="col-md-8 col-md-offset-2">
			<h2 className="headline_part centered margin-top-80">Comment ça marche ? <span className="margin-top-10">Discover & connect with great local businesses</span> </h2>
		  </div>
		</div>
		<div className="row container_icon"> 
		  <div className="col-md-4 col-sm-4 col-xs-12">
			<div className="box_icon_two box_icon_with_line"> <i className="im im-icon-Map-Marker2"></i>
			  <h3>Trouver un endroit intéressant</h3>
			  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque Nulla finibus.</p>
			</div>
		  </div>
		  
		  <div className="col-md-4 col-sm-4 col-xs-12">
			<div className="box_icon_two box_icon_with_line"> <i className="im im-icon-Mail-Add"></i>
			  <h3>Contacter quelques propriétaires</h3>
			  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque Nulla finibus.</p>
			</div>
		  </div>
		  
		  <div className="col-md-4 col-sm-4 col-xs-12">
			<div className="box_icon_two"> <i className="im im-icon-Administrator"></i>
			  <h3>Faire une réservation</h3>
			  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque Nulla finibus.</p>
			</div>
		  </div>
		</div>
	  </div>
  </section>
  
  <section className="fullwidth_block padding-top-75 padding-bottom-55" data-background-color="#fff"> 
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h3 className="headline_part centered margin-bottom-20">Choisissez votre plan<span>Découvrez les entreprises locales les mieux notées & mettez-vous en relation avec elles</span></h3>
        </div>
      </div>
      <div className="row">        
          <div className="utf_pricing_container_block margin-top-30 margin-bottom-20"> 
            <div className="plan featured col-md-3 col-sm-6 col-xs-12">
              <div className="utf_price_plan">
                <h3>Basic</h3>
                <span className="value">$29<span>/Par Mois</span></span> <span className="period">Abonnement Basic</span> 
			  </div>
              <div className="utf_price_plan_features">
                <ul>
                  <li>Frais uniques</li>
                  <li>Liste unique</li>
                  <li>Disponibilité de 90 jours</li>
                  <li>En vedette dans les résultats de recherche</li>
                  <li>Assistance 24/7</li>
                </ul>
                <a className="button border" href="#"><i className="sl sl-icon-basket"></i> Souscrire</a> 
			  </div>
            </div>
            
            <div className="plan featured col-md-3 col-sm-6 col-xs-12 active">
              <div className="utf_price_plan">
                <h3>Business</h3>
                <span className="value">$49<span>/Par Mois</span></span> <span className="period">Abonnement Business</span> </div>
              <div className="utf_price_plan_features">
                <ul>
                  <li>Frais uniques</li>
                  <li>Liste unique</li>
                  <li>Disponibilité de 90 jours</li>
                  <li>En vedette dans les résultats de recherche</li>
                  <li>Assistance 24/7</li>
                </ul>
                <a className="button" href="#"><i className="sl sl-icon-basket"></i> Souscrire</a> 
			  </div>
            </div>
            
			<div className="plan featured col-md-3 col-sm-6 col-xs-12">
              <div className="utf_price_plan">
                <h3>Premium</h3>
                <span className="value">$69<span>/Par Mois</span></span> <span className="period">Abonnement Premium</span> </div>
              <div className="utf_price_plan_features">
                <ul>
                  <li>Frais uniques</li>
                  <li>Liste unique</li>
                  <li>Disponibilité de 90 jours</li>
                  <li>En vedette dans les résultats de recherche</li>
                  <li>Assistance 24/7</li>
                </ul>
                <a className="button border" href="#"><i className="sl sl-icon-basket"></i> Souscrire</a> 
			  </div>
            </div>
			
            <div className="plan featured col-md-3 col-sm-6 col-xs-12">
              <div className="utf_price_plan">
                <h3>Platinum</h3>
                <span className="value">$99<span>/Par Mois</span></span> <span className="period">Abonnement Platinum</span> </div>
              <div className="utf_price_plan_features">
                <ul>
                  <li>Frais uniques</li>
                  <li>Liste unique</li>
                  <li>Disponibilité de 90 jours</li>
                  <li>En vedette dans les résultats de recherche</li>
                  <li>Assistance 24/7</li>
                </ul>
                <a className="button border" href="#"><i className="sl sl-icon-basket"></i> Souscrire</a> 
			  </div>
            </div>
          </div>        
      </div>      
    </div>    
  </section>
  
  <section className="fullwidth_block margin-bottom-0 padding-top-30 padding-bottom-65" dataBackgroundColor="linear-gradient(to bottom, #f9f9f9 0%, rgba(255, 255, 255, 1))">
	<div className="container">
		<div className="row">
			<div className="col-md-12">
				<h3 className="headline_part centered margin-bottom-40 margin-top-30">Nos partenaires</h3>
			</div>
			<div className="col-md-12">
				<div className="companie-logo-slick-carousel utf_dots_nav margin-bottom-10">
					<div className="item">
						<img src="images/brand_logo_01.png" alt="" />
					</div>
					<div className="item">
						<img src="images/brand_logo_02.png" alt="" />
					</div>
					<div className="item">
						<img src="images/brand_logo_03.png" alt="" />
					</div>
					<div className="item">
						<img src="images/brand_logo_04.png" alt="" />
					</div>
					<div className="item">
						<img src="images/brand_logo_05.png" alt="" />
					</div>		
					<div className="item">
						<img src="images/brand_logo_06.png" alt="" />
					</div>	
					<div className="item">
						<img src="images/brand_logo_07.png" alt="" />
					</div>					
				</div>
			</div>
		</div>
	</div>
  </section>
  
  <section className="utf_cta_area_item utf_cta_area2_block">
    <div className="container">
        <div className="row">
            <div className="col-md-12">
                <div className="utf_subscribe_block clearfix">
                    <div className="col-md-8 col-sm-7">
                        <div className="section-heading">
                            <h2 className="utf_sec_title_item utf_sec_title_item2">S'abonner à la Newsletter!</h2>
                            <p className="utf_sec_meta">
                                Inscrivez-vous pour recevoir les dernières mises à jour et informations.
                            </p>
                        </div>
                    </div>
                    <div className="col-md-4 col-sm-5">
                        <div className="contact-form-action">
                            <form method="post">
                                <span className="la la-envelope-o"></span>
                                <input className="form-control" type="email" placeholder="Entrez votre email" required="" />
                                <button className="utf_theme_btn" type="submit">S'abonner</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
	</section>
        </div>
    )
}