import { List } from '@material-ui/core'
import React, { useState,useEffect } from 'react'
import dynamic from 'next/dynamic'

const Map = dynamic (() => import ("../components/Layout/GoogleMaps"), { 
  loading: () => "Loading ...", 
  ssr: false 
});

const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/greggs.json?access_token=${process.env.MAPBOX_KEY}&bbox=-0.227654%2C51.464102%2C0.060737%2C51 .553421 & limit = 10`;

export default function Liste_annoncesl(){

  const [locations, setLocations] = useState([]);
  useEffect(() => {
    const fetchLocations = async () => {
      await fetch(url).then((response) =>
        response.text()).then((res) => JSON.parse(res))
      .then((json) => {
        setLocations(json.features);
      }).catch((err) => console.log({ err }));
    };
    fetchLocations();
  }, []);

    return(
        <div>
            
          <div>
            <div id="utf_map_container" className="fullwidth_block-home-map margin-bottom-30">
              {/* <Map locations={locations}/> */}
              <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d15889.760785347034!2d-3.998058452231979!3d5.349587275823977!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1scit%C3%A9%20des%20arts!5e0!3m2!1sfr!2sci!4v1621868626316!5m2!1sfr!2sci" width="100%" height="100%" style={{border:"0"}} allowfullscreen="" loading="lazy"></iframe>
              <div className="main_inner_search_block">
              <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="main_input_search_part">
                    <div className="main_input_search_part_item">
                      <input type="text" placeholder="Que rechercher-vous ?" value="" />
                    </div>
                    <div className="main_input_search_part_item intro-search-field">
                      <select className="utf_chosen_select_single" dataLiveSearch="true" dataSelectedTextFormat="count" dataSize="7" title="Sélectionnez le lieu">
                      <option>Afghanistan</option>
                      <option>Albania</option>
                      <option>Algeria</option>
                      <option>Brazil</option>
                      <option>Burundi</option>
                      <option>Bulgaria</option>
                      <option>Germany</option>
                      <option>Grenada</option>
                      <option>Guatemala</option>
                      <option>Iceland</option>
                      </select>
                    </div>
                    <div className="main_input_search_part_item intro-search-field">
                      <select dataPlaceholder="Toutes Categories" className="utf_chosen_select_single" title="Toutes Categories" dataLiveSearch="true" dataSelectedTextFormat="count" dataSize="7">
                        <option>Food & Restaurants </option>
                        <option>Shop & Education</option>
                        <option>Education</option>
                        <option>Business</option>
                        <option>Events</option>
                      </select>
                    </div>
                    <button className="button">Recherche</button>
                  </div>
                </div>
              </div>
              </div>
              </div>
              <a href="#" id="scrollEnabling" title="Enable or disable scrolling on map">Défilement activé</a> 
            </div>
          </div>
  <div className="container">
    <div className="row">
	  <div className="col-md-12">
        <div className="listing_filter_block margin-top-30">
          <div className="col-md-3 col-xs-3">
            <div className="utf_layout_nav"> <a href="/Liste_annonces_grid" className="grid"><i className="fa fa-th"></i></a> <a href="/Liste_annonces_list" className="list active"><i className="fa fa-align-justify"></i></a> </div>
          </div>
         
        </div>
        <div className="row">
          <div className="col-lg-12 col-md-12">
            <div className="utf_listing_item-container list-layout"> <a href="/Detail_annonces" className="utf_listing_item">
              <div className="utf_listing_item-image"> 
				  <img src="images/hotels.jpg" alt="" /> 
				  <span className="like-icon"></span> 
				  <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span> 
				  <span className="featured_tag">Featured</span> 
				  <div className="utf_listing_prige_block utf_half_list">							
					<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>					
					<span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
				  </div>
			  </div>
			  <span className="utf_open_now">Ouvert</span>
              <div className="utf_listing_item_content">
                <div className="utf_listing_item-inner">
                  <h3>Chontaduro Barcelona</h3>
                  <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
				  <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
                  <div className="utf_star_rating_section" data-rating="4.5">
                    <div className="utf_counter_star_rating">(4.5)</div>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt.</p>
                </div>
              </div>
              </a> 
			</div>
          </div>
          <div className="col-lg-12 col-md-12">
            <div className="utf_listing_item-container list-layout"> <a href="/Detail_annonces" className="utf_listing_item">
              <div className="utf_listing_item-image"> 
				  <img src="images/hotels.jpg" alt="" /> 
				  <span className="like-icon"></span> 
				  <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span> 
				  <div className="utf_listing_prige_block utf_half_list">							
					<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>					
					<span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
				  </div>
			  </div>
              <div className="utf_listing_item_content">
                <div className="utf_listing_item-inner">
                  <h3>The Lounge & Bar</h3>
                  <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
				  <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
                  <div className="utf_star_rating_section" data-rating="5.0">
                    <div className="utf_counter_star_rating">(5)</div>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt.</p>
                </div>
              </div>
              </a> 
			</div>
          </div>
          <div className="col-lg-12 col-md-12">
            <div className="utf_listing_item-container list-layout"> <a href="/Detail_annonces" className="utf_listing_item">
              <div className="utf_listing_item-image"> 
                <img src="images/hotels.jpg" alt="" /> 
                <span className="like-icon"></span> 
                <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span> 
                <div className="utf_listing_prige_block utf_half_list">							
                  <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>					
                  <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                </div>
              </div>
              <div className="utf_listing_item_content">
                <div className="utf_listing_item-inner">
                  <h3>Westfield Sydney</h3>
                  <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
				          <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
                  <div className="utf_star_rating_section" data-rating="2.0">
                    <div className="utf_counter_star_rating">(2.0)</div>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt.</p>
                </div>
              </div>
              </a> 
			      </div>
          </div>
          <div className="col-lg-12 col-md-12">
            <div className="utf_listing_item-container list-layout"> <a href="/Detail_annonces" className="utf_listing_item">
              <div className="utf_listing_item-image"> 
				<img src="images/hotels.jpg" alt="" /> 
				<span className="like-icon"></span> 
				<span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span> 
				<span className="featured_tag">Featured</span> 
				<div className="utf_listing_prige_block utf_half_list">							
					<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>					
					<span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
				  </div>
			  </div>
			  <span className="utf_closed">Closed</span>
              <div className="utf_listing_item_content">
                <div className="utf_listing_item-inner">
                  <h3>Ruby Beauty Center</h3>
                  <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
				          <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
                  <div className="utf_star_rating_section" data-rating="5.0">
                    <div className="utf_counter_star_rating">(5)</div>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt.</p>
                </div>
              </div>
              </a> 
			      </div>
          </div>
          <div className="col-lg-12 col-md-12">
            <div className="utf_listing_item-container list-layout"> <a href="/Detail_annonces" className="utf_listing_item">
              <div className="utf_listing_item-image"> 
                <img src="images/hotels.jpg" alt="" /> 
                <span className="like-icon"></span> 
                <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span> 
                <div className="utf_listing_prige_block utf_half_list">							
                <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>					
                <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                </div>
              </div>
              <div className="utf_listing_item_content">
                <div className="utf_listing_item-inner">
                  <h3>UK Fitness Club</h3>
                  <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
				          <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
                  <div className="utf_star_rating_section" data-rating="4.5">
                    <div className="utf_counter_star_rating">(4.5)</div>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt.</p>
                </div>
              </div>
              </a> 
			      </div>
          </div>
          <div className="col-lg-12 col-md-12">
            <div className="utf_listing_item-container list-layout"> <a href="/Detail_annonces" className="utf_listing_item">
              <div className="utf_listing_item-image"> 
                <img src="images/hotels.jpg" alt="" /> 
                <span className="like-icon"></span> 
                <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span> 
                <span className="featured_tag">Featured</span>
                <div className="utf_listing_prige_block utf_half_list">							
                <span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>					
                <span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
                </div>	
              </div>
              <div className="utf_listing_item_content">
                <div className="utf_listing_item-inner">
                  <h3>Fairmont Pacific Rim</h3>
                  <span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>
				          <span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
                  <div className="utf_star_rating_section" data-rating="5.0">
                    <div className="utf_counter_star_rating">(4.5)</div>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt.</p>
                </div>
              </div>
              </a> 
			      </div>
          </div>
        </div>
        <div className="clearfix"></div>
        <div className="row">
          <div className="col-md-12">
            <div className="utf_pagination_container_part margin-top-20 margin-bottom-75">
              <nav className="pagination">
                <ul>
                  <li><a href="#"><i className="sl sl-icon-arrow-left"></i></a></li>
                  <li><a href="#" className="current-page">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#"><i className="sl sl-icon-arrow-right"></i></a></li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section className="utf_cta_area_item utf_cta_area2_block">
    <div className="container">
        <div className="row">
            <div className="col-md-12">
                <div className="utf_subscribe_block clearfix">
                    <div className="col-md-8 col-sm-7">
                        <div className="section-heading">
                            <h2 className="utf_sec_title_item utf_sec_title_item2">S'abonner à la Newsletter!</h2>
                            <p className="utf_sec_meta">
                                Inscrivez-vous pour recevoir les dernières mises à jour et informations.
                            </p>
                        </div>
                    </div>
                    <div className="col-md-4 col-sm-5">
                        <div className="contact-form-action">
                            <form method="post">
                                <span className="la la-envelope-o"></span>
                                <input className="form-control" type="email" placeholder="Entrez votre email" required="" />
                                <button className="utf_theme_btn" type="submit">S'abonner</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
	</section>
        </div>
    )
}