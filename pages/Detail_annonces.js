import React from 'react'
import DetailsCarousel from '../components/Layout/DetailsCarousel'
import OurDropdown from '../components/Layout/OurDropdown'
import OurDropdownTwo from '../components/Layout/DropdownTwo'

import 'date-fns';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';
import DirectionsWalkIcon from '@material-ui/icons/DirectionsWalk';
import DirectionsBikeIcon from '@material-ui/icons/DirectionsBike';
import { makeStyles } from '@material-ui/core/styles';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
  },
  dropdown: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    zIndex: 99999999,
    padding: theme.spacing(1),
    backgroundColor: theme.palette.background.paper,
  },
}));


export default function Detail_annonces(){
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen((prev) => !prev);
  };

  const handleClickAway = () => {
    setOpen(false);
  };


  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
		const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'));

		const handleDateChange = (date) => {
		setSelectedDate(date);
		};
    return(
        <div>
            <div id="utf_listing_gallery_part" className="utf_listing_section">
				      <DetailsCarousel/>
            </div>
      <div className="container">
        <div className="row utf_sticky_main_wrapper">
          <div className="col-lg-8 col-md-8">
            <div id="titlebar" className="utf_listing_titlebar">
              <div className="utf_listing_titlebar_title">
              <h2>La pommade <span className="listing-tag">Beauté</span></h2>
                <span> <a href="#utf_listing_location" className="listing-address"> <i className="sl sl-icon-location"></i> Abidjan, Cocody angré soleil 3 </a> </span>			
                <span className="call_now"><i className="sl sl-icon-phone"></i> (+225) 0102030405</span>
                
              </div>
            </div>
            <section class="fullwidth_block padding-bottom-70" data-background-color="#f9f9f9" style={{background: 'rgb(249, 249, 249)', padding:'10px'}}> 
              <div class="">
              <div class="row">
                <div class="col-md-8 col-md-offset-2"> 
                  <h2 class="headline_part centered margin-top-80">Nos services
                    <span class="margin-top-10">Avant de commencer, veuillez aussi lire nos <a href="#" style={{color:'#ff2222'}}>Termes et Conditions</a></span> 
                    
                  </h2>
                </div>
              </div>
              <ClickAwayListener onClickAway={handleClickAway}>
              <div class="row container_icon"> 
                <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="box_icon_two box_icon_with_line"> <i class="im im-icon-Pen"></i>
                  
                    <div>
                      <button type="button" style={{backgroundColor:'transparent', border:'none'}} onClick={handleClick}>
                      Remplissez le formulaire ci-dessous
                      </button>
                      {open ? (
                        <div className={classes.dropdown}>
                          <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                        </div>
                      ) : null}
                    </div>
                </div>
                </div>
                
                <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="box_icon_two box_icon_with_line"> <i class="im im-icon-MaleFemale"></i>
          
                    <div >
                      <button type="button" style={{backgroundColor:'transparent', border:'none'}} onClick={handleClick}>
                        Une équipe analysera votre demande de financement
                      </button>
                      {open ? (
                        <div className={classes.dropdown}>
                          <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                        </div>
                      ) : null}
                    </div>
                </div>
                </div>
                
                <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="box_icon_two"> <i class="im im-icon-Headset"></i>
                    <div>
                      <button type="button" style={{backgroundColor:'transparent', border:'none'}} onClick={handleClick}>
                      Nous vous contacterons dans de brefs délais
                      </button>
                      {open ? (
                        <div className={classes.dropdown}>
                          <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                        </div>
                      ) : null}
                    </div>
                </div>
                </div>
              </div>
              </ClickAwayListener>
              </div>
            </section>

            <section className="fullwidth_block margin-top-65 padding-top-45 p-4 padding-bottom-45" style={{backgroundColor:"#e2e2e2"}}>
              <div className="container-fluid">
                <div className="row slick_carousel_slider">
                  <div className="col-md-12">
                    <h3 className="headline_part margin-bottom-45" style={{textDecoration:'underline', color:'#54ba1d'}}> Explorer le site </h3>
                  </div>
              
                  <div style={{margin:'auto'}}>
                    <div className="row">
                      <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                          <div className="card box_icon_two p-2 m-0" style={{minHeight:'10px'}}>
                            <a href="images/hotels.jpg">
                              <img src="images/hotels.jpg" alt=""/>
                            </a>
                          </div>
                      </div>
                      <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="row">
                          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div className="card box_icon_two p-2 m-0 mb-2" style={{minHeight:'10px'}}>
                              <a href="images/hotels.jpg">
                                <img src="images/hotels.jpg" alt=""/>
                              </a>
                            </div>
                          </div>
                          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div className="card box_icon_two p-2 m-0 mb-2" style={{minHeight:'10px'}}>
                              <a href="images/hotels.jpg">
                                <img src="images/hotels.jpg" alt=""/>
                              </a>
                            </div>
                          </div>
                          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div className="card box_icon_two p-2 m-0 mb-2" style={{minHeight:'10px'}}>
                              <a href="images/hotels.jpg">
                                <img src="images/hotels.jpg" alt=""/>
                              </a>
                            </div>
                          </div>
                          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div className="card box_icon_two p-2 m-0 mb-2" style={{minHeight:'10px'}}>
                              <a href="images/hotels.jpg">
                                <img src="images/hotels.jpg" alt=""/>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
                      
        
        <div id="utf_listing_amenities" className="utf_listing_section">
          <h3 className="utf_listing_headline_part margin-top-50 margin-bottom-40">Commodités</h3>
          <ul className="utf_listing_features checkboxes margin-top-0">
            <li>Elevator in Building</li>
            <li>Friendly Workspace</li>
            <li>Instant Book</li>
            <li>Wireless Internet</li>
            <li>Free Parking on Premises</li>
            <li>Free Parking on Street</li>
            <li>Live Music</li>
            <li>Accepting Credit Cards</li>
            <li>Air Conditioned</li>
            <li>Satellite TV</li>
            <li>Coffeemaker</li>
            <li>Hair Dryer</li>
            <li>Swimming Pool</li>
            <li>Room Service</li>
            <li>Luxury Bedding</li>
            <li>Good Showers</li>
            <li>Free Parking</li>
            <li>Free Wifi</li>
            <li>Bath Towel</li>
            <li>Free Coffee</li>
            <li>Pets Allow</li>
            <li>Hot Water</li>
            <li>Attached Garage</li>
            <li>Elevator</li>
            <li>Spa/Sauna</li>
            <li>Indoor Pool</li>
            <li>Security Cameras</li>
          </ul>
        </div>
        
		
        <div id="utf_listing_reviews" className="utf_listing_section">
          <h3 className="utf_listing_headline_part margin-top-75 margin-bottom-20">Commentaires <span>(228)</span></h3>
          <div className="clearfix"></div>
		  <div className="reviews-container">
			<div className="row">
			
				
			</div>
		  </div>
          <div className="comments utf_listing_reviews">
            <ul>
              <li>
                <div className="avatar"><img src="images/client-avatar1.jpg" alt="" /></div>
                <div className="utf_comment_content">
                  <div className="utf_arrow_comment"></div>
                  <div className="utf_star_rating_section" data-rating="5"></div>
				  <a href="#" className="rate-review">Helpful Review <i className="fa fa-thumbs-up"></i></a>
                  <div className="utf_by_comment">Francis Burton<span className="date"><i className="fa fa-clock-o"></i> Jan 05, 2019 - 8:52 am</span> </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat.</p>                                    
				</div>
              </li>
              <li>
                <div className="avatar"><img src="images/client-avatar2.jpg" alt="" /> </div>
                <div className="utf_comment_content">
                  <div className="utf_arrow_comment"></div>
                  <div className="utf_star_rating_section" data-rating="4"></div>
				  <a href="#" className="rate-review">Helpful Review <i className="fa fa-thumbs-up"></i></a>
                  <div className="utf_by_comment">Francis Burton<span className="date"><i className="fa fa-clock-o"></i> Jan 05, 2019 - 8:52 am</span> </div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat.</p>                  
				</div>
              </li>
			  <li>
                <div className="avatar"><img src="images/client-avatar3.jpg" alt="" /> </div>
                <div className="utf_comment_content">
                  <div className="utf_arrow_comment"></div>
                  <div className="utf_star_rating_section" data-rating="4"></div>                  
                  <div className="utf_by_comment">Francis Burton<span className="date"><i className="fa fa-clock-o"></i> Jan 05, 2019 - 8:52 am</span> </div>
				  <a href="#" className="rate-review">Helpful Review <i className="fa fa-thumbs-up"></i></a>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat.</p>                  
				</div>
              </li>
              <li>
                <div className="avatar"><img src="images/client-avatar1.jpg" alt="" /></div>
                <div className="utf_comment_content">
                  <div className="utf_arrow_comment"></div>
                  <div className="utf_star_rating_section" data-rating="4.5"></div>                  
                  <div className="utf_by_comment">Francis Burton<span className="date"><i className="fa fa-clock-o"></i> Jan 05, 2019 - 8:52 am</span> </div>
				          <a href="#" className="rate-review">Helpful Review <i className="fa fa-thumbs-up"></i></a>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat.</p>
                  <div className="review-images utf_gallery_container"> 
				    <a href="images/review-image-01.jpg" className="utf_gallery"><img src="images/review-image-01.jpg" alt="" /></a> 
					<a href="images/review-image-02.jpg" className="utf_gallery"><img src="images/review-image-02.jpg" alt="" /></a> 
					<a href="images/review-image-03.jpg" className="utf_gallery"><img src="images/review-image-03.jpg" alt="" /></a> 
					<a href="images/review-image-01.jpg" className="utf_gallery"><img src="images/review-image-01.jpg" alt="" /></a> 
					<a href="images/review-image-02.jpg" className="utf_gallery"><img src="images/review-image-02.jpg" alt="" /></a> 
					<a href="images/review-image-03.jpg" className="utf_gallery"><img src="images/review-image-03.jpg" alt="" /></a> 
				  </div>                  
				</div>
              </li>
			  <li>
                <div className="avatar"><img src="images/client-avatar3.jpg" alt="" /> </div>
                <div className="utf_comment_content">
                  <div className="utf_arrow_comment"></div>
                  <div className="utf_star_rating_section" data-rating="4"></div>                  
                  <div className="utf_by_comment">Francis Burton<span className="date"><i className="fa fa-clock-o"></i> Jan 05, 2019 - 8:52 am</span> </div>
				  <a href="#" className="rate-review">Helpful Review <i className="fa fa-thumbs-up"></i></a>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat.</p>                  
				</div>
              </li>
            </ul>
          </div>
          <div className="clearfix"></div>
          <div className="row">
            <div className="col-md-12">
              <div className="utf_pagination_container_part margin-top-30">
                <nav className="pagination">
                  <ul>
                    <li><a href="#"><i className="sl sl-icon-arrow-left"></i></a></li>
                    <li><a href="#" className="current-page">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#"><i className="sl sl-icon-arrow-right"></i></a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          <div className="clearfix"></div>
        </div>
        <div id="utf_add_review" className="utf_add_review-box">
          <h3 className="utf_listing_headline_part margin-bottom-20">Commenter</h3>
          <span className="utf_leave_rating_title">Your email address will not be published.</span>
          <div className="row">
            <div className="col-md-6 col-sm-6 col-xs-12">
              <div className="clearfix"></div>
              <div className="utf_leave_rating margin-bottom-30">
                <input type="radio" name="rating" id="rating-1" value="1"/>
                <label for="rating-1" className="fa fa-star"></label>
                <input type="radio" name="rating" id="rating-2" value="2"/>
                <label for="rating-2" className="fa fa-star"></label>
                <input type="radio" name="rating" id="rating-3" value="3"/>
                <label for="rating-3" className="fa fa-star"></label>
                <input type="radio" name="rating" id="rating-4" value="4"/>
                <label for="rating-4" className="fa fa-star"></label>
                <input type="radio" name="rating" id="rating-5" value="5"/>
                <label for="rating-5" className="fa fa-star"></label>
              </div>
              <div className="clearfix"></div>
            </div>
            <div className="col-md-6 col-sm-6 col-xs-12">
              <div className="add-review-photos margin-bottom-30">
                <div className="photoUpload"> <span>Upload Photo <i className="sl sl-icon-arrow-up-circle"></i></span>
                  <input type="file" className="upload" />
                </div>
              </div>
            </div>
          </div>
          <form id="utf_add_comment" className="utf_add_comment">
            <fieldset>
              <div className="row">
                <div className="col-md-4">
                  <label>Name:</label>
                  <input type="text" placeholder="Name" value=""/>
                </div>
                <div className="col-md-4">
                  <label>Email:</label>
                  <input type="text" placeholder="Email" value=""/>
                </div>
                <div className="col-md-4">
                  <label>Subject:</label>
                  <input type="text" placeholder="Subject" value=""/>
                </div>
              </div>
              <div>
                <label>Review:</label>
                <textarea cols="40" placeholder="Your Message..." rows="3"></textarea>
              </div>
            </fieldset>
            <button className="button">Submit Review</button>
            <div className="clearfix"></div>
          </form>
        </div>
      </div>
      

      <div className="col-lg-4 col-md-4 margin-top-75 sidebar-search">
        <a href="#">
        <div className="verified-badge with-tip margin-bottom-30" data-tip-content="Listing has been verified and belongs business owner or manager."> <i className="sl sl-icon-check"></i> S'abonner</div>
        </a>
        <div style={{padding:'20px', backgroundColor:'#f9f9f9'}} className="margin-bottom-30">
          <div className="row">
            <div className="col-3"></div>
            <div className="col-2">
              <div className="icon_direct">
                <li><a href="#"><DirectionsCarIcon style={{ fontSize: 30 }} /></a></li>
              </div>
            </div>
            <div className="col-2">
              <div className="icon_direct">
                <li><a href="#"><DirectionsWalkIcon style={{ fontSize: 30 }} /></a></li>
              </div>
            </div>
            <div className="col-2">
              <div className="icon_direct">
                <li><a href="#"><DirectionsBikeIcon style={{ fontSize: 30 }} /></a></li>
              </div>
            </div>
            <div className="col-3"></div>
          </div> <br/>
          <div className="row">
            <div className="col-lg-1 m-auto">
              <div style={{height:'15px',border:'2px solid black', width:'15px',borderRadius:'50%', backgroundColor:'#54ba1d',margin:'auto', position: 'absolute',right:'0',bottom: '0px'}}></div>
            </div>
            <div className="col-lg-10">
                <input type="text" name="nom" placeholder="Veuillez saisir votre Position Actuelle" />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-1 m-auto">
              <div style={{height:'15px',border:'2px solid black', width:'15px',borderRadius:'50%', backgroundColor:'#f98925',margin:'auto', position: 'absolute',right:'0',bottom: '0px'}}></div>
            </div>
          <div className="col-lg-10">
              <input type="text" name="lieu" placeholder="Veuillez saisir votre Lieu choisi" />
          </div>
          </div>

          <div>
            <p style={{fontSize:'18px'}}>Temps estimé à : <span style={{color: '#54ba1d',fontWeight:'bold'}}>03heure 30minute</span></p>
          </div>

        </div>
        <div className="utf_box_widget booking_widget_box">
          <h3><i className="fa fa-calendar"></i> Prenez un rendez-vous</h3>
          <div className="row with-forms margin-top-0">
          <div className="col-lg-12">
              <input type="text" name="nom" placeholder="Veuillez saisir votre nom" />
          </div>
          <div className="col-lg-12">
            <input type="tel" name="nom" placeholder="Veuillez saisir votre numéro" />
          </div>
            <div className="col-lg-12 col-md-12 select_date_box">
			  	<MuiPickersUtilsProvider utils={DateFnsUtils}>
					<Grid container justify="space-around" id="date-picker">
						<KeyboardDatePicker
						disableToolbar
						variant="inline"
						format="MM/dd/yyyy"
						id="date-picker-inline"
						value={selectedDate}
						onChange={handleDateChange}
						KeyboardButtonProps={{
							'aria-label': 'change date',
						}}
						/>
					</Grid>
				</MuiPickersUtilsProvider>
            </div>
  		    
          </div>          
          <a href="listing_booking.html" className="utf_progress_button button fullwidth_block margin-top-5">Prendre un RDV</a>
          <div className="clearfix"></div>
        </div>
        <div className="utf_box_widget margin-top-35">
          <h3><i className="sl sl-icon-phone"></i> A propos de nous</h3>
          <div className="utf_hosted_by_user_title"> <a href="#" className="utf_hosted_by_avatar_listing"><img src="images/yèrè2babi.png" alt="" /></a>
            <h4><a href="#">Cecilia Koffi</a><span>Gérante</span>
              <span><i className="sl sl-icon-location"></i> Abidjan, Cocody</span>
            </h4>
          </div>
		      <ul className="utf_social_icon rounded margin-top-10">
            <li><a className="facebook" href="#"><i className="icon-facebook"></i></a></li>
            <li><a className="twitter" href="#"><i className="icon-twitter"></i></a></li>
            <li><a className="gplus" href="#"><i className="icon-gplus"></i></a></li>
            <li><a className="linkedin" href="#"><i className="icon-linkedin"></i></a></li>
            <li><a className="instagram" href="#"><i className="icon-instagram"></i></a></li>            
          </ul>
          <ul className="utf_listing_detail_sidebar">
            <li><i className="sl sl-icon-map"></i> CI, Abidjan, Cocody angré soleil 3</li>
            <li><i className="sl sl-icon-phone"></i> +(225) 0102030405</li>
            <li><i className="sl sl-icon-globe"></i> <a href="#">www.lapommade.com</a></li>
            <li><i className="fa fa-envelope-o"></i> <a href="mailto:info@example.com">lapommade@gmail.com</a></li>
          </ul>		  
        </div>	
		
		<div className="utf_box_widget opening-hours margin-top-35">
          <h3><i className="sl sl-icon-envelope-open"></i> Laissez nous un message</h3>
          <form id="contactform">
            <div className="row">              
              <div className="col-md-12">                
                  <input name="name" type="text" placeholder="Votre nom" required="" />                
              </div>
              <div className="col-md-12">                
                  <input name="email" type="email" placeholder="Votre email" required="" />                
              </div>      
			  <div className="col-md-12">                
                  <input name="phone" type="text" placeholder="Votre numero de téléphone" required="" />                
              </div>	
			  <div className="col-md-12">
				  <textarea name="comments" cols="40" rows="2" id="comments" placeholder="Saisissez votre message" required=""></textarea>
			  </div>
            </div>            
            <input type="submit" className="submit button" id="submit" value="Envoyez votre message" />
          </form>
        </div>
        <div className="p-4 margin-top-35" style={{backgroundColor:'#54ba1d'}}>

        </div>
      </div>
    </div>
  </div>
  
  <section className="utf_cta_area_item utf_cta_area2_block">
    <div className="container">
        <div className="row">
            <div className="col-md-12">
                <div className="utf_subscribe_block clearfix">
                    <div className="col-md-8 col-sm-7">
                        <div className="section-heading">
                            <h2 className="utf_sec_title_item utf_sec_title_item2">S'abonner à la Newsletter!</h2>
                            <p className="utf_sec_meta">
                                Inscrivez-vous pour recevoir les dernières mises à jour et informations.
                            </p>
                        </div>
                    </div>
                    <div className="col-md-4 col-sm-5">
                        <div className="contact-form-action">
                            <form method="post">
                                <span className="la la-envelope-o"></span>
                                <input className="form-control" type="email" placeholder="Entrez votre email" required="" />
                                <button className="utf_theme_btn" type="submit">S'abonner</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
	</section>
        </div>
    )
}