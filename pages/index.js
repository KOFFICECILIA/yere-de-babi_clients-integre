import Head from 'next/head'
import styles from '../styles/Home.module.css'
import SwipeableTextMobileStepper from '../components/Layout/Slider'
import SectionCarousel from '../components/Layout/Carousel'
import TestimoCarousel from '../components/Layout/TestimoCarousel'
import AnnoncesCarousel from '../components/Layout/AnnoncesCarousel'
import PaternCarousel from '../components/Layout/PaternCarousel'


export default function Home() {
  return (
    <div>
      <div className="clearfix"></div>
      <div id="utf_rev_slider_wrapper" className="rev_slider_wrapper fullwidthbanner-container" data-alias="classNameicslider1" style={{margin:"0px auto", backgroundColor:"transparent", padding:"0px", marginTop:"0px", marginBottom:"0px"}}> 
      <SectionCarousel/>
  </div>
  
  <div className="container">
	<div className="row">
      <div className="col-md-12">
        <h3 className="headline_part centered margin-top-75"> Catégories les plus populaires<span>Cghoisissez où manger, quoi voir et comment vous amuser</span></h3>
      </div>
    </div>
    <div className="row">
      <div className="col-md-12">
        <div className="container_categories_box margin-top-5 margin-bottom-30"> 
          <a href="/Liste_annonces_list" className="utf_category_small_box_part"> <i className="im im-icon-Hotel"></i>
			<h4>Hôtels</h4>
			<span>22</span>
          </a> 
          <a href="/Liste_annonces_list" className="utf_category_small_box_part"> <i className="im im-icon-Hamburger"></i>
			<h4>Nourritures & Boissons</h4>
			<span>15</span>
          </a> 
          <a href="/Liste_annonces_list" className="utf_category_small_box_part"> <i className="im im-icon-Shop-2"></i>
			<h4>Boutiques</h4>
			<span>05</span>
          </a> 
          <a href="/Liste_annonces_list" className="utf_category_small_box_part"> <i className="im im-icon-Cocktail"></i>
			<h4>Vie nocturne</h4>
			<span>12</span>
          </a> 
          <a href="/Liste_annonces_list" className="utf_category_small_box_part"> <i className="im im-icon-Electric-Guitar"></i>
			<h4>Événements</h4>
			<span>08</span>
          </a> 
          <a href="/Liste_annonces_list" className="utf_category_small_box_part"> <i className="im im-icon-Dumbbell"></i>
			<h4>Fitness</h4>
			<span>18</span>
          </a> 
          <a href="/Liste_annonces_list" className="utf_category_small_box_part"> <i className="im im-icon-Home-2"></i>
			<h4>Immobilier</h4>
			<span>14</span>
          </a> 
          <a href="/Liste_annonces_list" className="utf_category_small_box_part"> <i className="im im-icon-Chef-Hat"></i>
			<h4>Restaurants</h4>
			<span>22</span>
          </a> 
          <a href="/Liste_annonces_list" className="utf_category_small_box_part"> <i className="im im-icon-Couple-Sign"></i>
			<h4>Piste de danse</h4>
			<span>20</span>
          </a> 
          <a href="/Liste_annonces_list" className="utf_category_small_box_part"> <i className="im im-icon-Old-Cassette"></i>
			<h4>Cinéma</h4>
			<span>07</span>
          </a> 
		</div>
		<div className="col-md-12 centered_content"> <a href="/Categorie" className="button border margin-top-20">Voir plus</a> </div>
      </div>
    </div>
  </div>

  <a href="/Liste_annonces_grid" className="flip-banner parallax" data-color="#000" data-color-opacity="0.85" data-img-width="2500" style={{height:"500px"}}>
  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d15889.760785347034!2d-3.998058452231979!3d5.349587275823977!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1scit%C3%A9%20des%20arts!5e0!3m2!1sfr!2sci!4v1621868626316!5m2!1sfr!2sci" width="100%" height="100%" style={{border:"0"}} allowfullscreen="" loading="lazy"></iframe>
	  <div className="flip-banner-content">
		<h2 className="flip-visible">EXPLORER NOTRE MAP YERE2BABI</h2>    
	  </div>
  </a>

  <div className="container padding-bottom-70">
    <div className="row">
      <div className="col-md-12">
        <h3 className="headline_part centered margin-bottom-35 margin-top-70">Villes les plus populaires <span>Découvrez les meilleures choses à faire - restaurants, shopping, hôtels, cafés et lieux<br/>de côte d'ivoire par catégories.</span></h3>
      </div>
      <div className="col-md-3"> 
         <a href="/Liste_annonces_list" className="img-box" style={{background:"url('images/nightc.jpg')", backgroundSize:"cover", backgroundPosition:"center"}}>
          <div className="utf_img_content_box visible">
            <h4>Nightlife </h4>
            <span>18 Listings</span> 
          </div>
         </a> 
	    </div>
      <div className="col-md-3"> 
         <a href="/Liste_annonces_list" className="img-box" style={{background:"url('images/shops.jpg')", backgroundSize:"cover", backgroundPosition:"center"}}>
			<div className="utf_img_content_box visible">
			  <h4>Shops</h4>
			  <span>24 Listings</span> 
			</div>
         </a> 
	  </div>
      <div className="col-md-6"> 
         <a href="/Liste_annonces_list" className="img-box" style={{background:"url('images/popular-location-03.jpg')", backgroundSize:"cover", backgroundPosition:"center"}}>
			<div className="utf_img_content_box visible">
			  <h4>Restaurant</h4>
			  <span>17 Listings</span> 
			</div>
         </a> 
	  </div>
      <div className="col-md-6"> 
         <a href="/Liste_annonces_list" className="img-box" style={{background:"url('images/outdoor_act.jpg')", backgroundSize:"cover", backgroundPosition:"center"}}>
			<div className="utf_img_content_box visible">
			  <h4>Outdoor Activities</h4>
			  <span>12 Listings</span> 
			</div>
         </a> 
	  </div>
      <div className="col-md-3"> 
         <a href="/Liste_annonces_list" className="img-box" style={{background:"url('images/hotels.jpg')", backgroundSize:"cover", backgroundPosition:"center"}}>
			<div className="utf_img_content_box visible">
			  <h4>Hotels</h4>
			  <span>14 Listings</span> 
			</div>
         </a> 
	  </div>
      <div className="col-md-3"> 
         <a href="/Liste_annonces_list" className="img-box" style={{background:"url('images/beach.jpg')", backgroundSize:"cover", backgroundPosition:"center"}}>
          <div className="utf_img_content_box visible">
            <h4>Bassam</h4>
            <span>9 Listings</span> 
          </div>
         </a>
	  </div>
	  <div className="col-md-12 centered_content"> <a href="#" className="button border margin-top-20">Voir plus Categories</a> </div>
    </div>
  </div>
  
{/* <section className="fullwidth_block margin-top-65 padding-top-75 padding-bottom-70" style={{backgroundColor:"#e2e2e2"}}>
  <div className="container-fluid">
    <div className="row slick_carousel_slider">
      <div className="col-md-12">
        <h3 className="headline_part centered margin-bottom-45"> Lieux les plus visités <span>Explorez les plus beaux endroits de la ville</span> </h3>
      </div>
  
      <div style={{margin:'auto'}}>
        <div>
          <div className="Anno_slider" style={{minHeight:"480px"}}> 
            <AnnoncesCarousel/>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> */}
  
  
  
  <section className="utf_testimonial_part fullwidth_block padding-top-75 padding-bottom-75"> 
    <div className="container">
      <div className="row">
        <div className="col-md-8 col-md-offset-2">
          <h3 className="headline_part centered">Avis de nos clients <span className="margin-top-15">Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since...</span> </h3>
        </div>
      </div>
    </div>
    <div className="fullwidth_carousel_container_block Anno_slider">
      <div className="container-fluid" style={{margin:'auto'}}> 
      	<TestimoCarousel />
      </div>
    </div>
  </section>   
  
  <section className="fullwidth_block margin-bottom-0 padding-top-30 padding-bottom-65" style={{background:"linear-gradient(to bottom, #f9f9f9 0%, rgba(255, 255, 255, 1))"}}>
	<div className="container">
		<div className="">
			<div className="col-md-12">
				<h3 className="headline_part centered margin-bottom-40 margin-top-30">Nos partenaires</h3>
			</div>
      <PaternCarousel/>
		</div>
	</div>
  </section>
  
  <section className="utf_cta_area_item utf_cta_area2_block">
    <div className="container">
        <div className="row">
            <div className="col-md-12">
                <div className="utf_subscribe_block clearfix">
                    <div className="col-md-8 col-sm-7">
                        <div className="section-heading">
                            <h2 className="utf_sec_title_item utf_sec_title_item2">S'abonner à la Newsletter!</h2>
                            <p className="utf_sec_meta">
                                Inscrivez-vous pour recevoir les dernières mises à jour et informations.
                            </p>
                        </div>
                    </div>
                    <div className="col-md-4 col-sm-5">
                        <div className="contact-form-action">
                            <form method="post">
                                <span className="la la-envelope-o"></span>
                                <input className="form-control" type="email" placeholder="Entrez votre email" required="" />
                                <button className="utf_theme_btn" type="submit">S'abonner</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
	</section>  

  

    </div>
    
  )

  
}
