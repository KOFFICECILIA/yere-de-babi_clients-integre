import React from 'react'

export default function Categorie(){
    return(
        <div>
            <div id="titlebar" className="gradient">
		<div className="container">
		  <div className="row">
			<div className="col-md-12">
			  <h2>Categories</h2>
			  <nav id="breadcrumbs">
				<ul>
				  <li><a href="/"> Accueil</a></li>
				  <li>tries par catégories</li>
				</ul>
			  </nav>
			</div>
		  </div>
		</div>
	</div>

	<div className="container">
		<div className="row">
			<div className="col-md-12">
				<div id="filters">
					<ul className="option-set margin-bottom-30">
						<li><a href="#filter" className="selected" data-filter="*">Tout</a></li>
						<li><a href="#filter" data-filter=".restaurants">Restaurants</a></li>
						<li><a href="#filter" data-filter=".food_drinks">Nourritures & Boissons</a></li>
						<li><a href="#filter" data-filter=".travels">Voyages</a></li>
						<li><a href="#filter" data-filter=".events">Evènements</a></li>
					</ul>
					<div className="clearfix"></div>
				</div>
			</div>
		</div>
		<div className="row">
			<div className="projects isotope-wrapper">
				<div className="col-md-4 col-lg-4 col-md-12 isotope-item events restaurants"> <a href="/Detail_annonces" className="utf_listing_item-container" data-marker-id="1">
					<div className="utf_listing_item"> <img src="images/utf_listing_item-01.jpg" alt="" /> <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span> <span className="featured_tag">Featured</span>
					  <span className="utf_closed">Closed</span>
					  <div className="utf_listing_item_content"> 
						<div className="utf_listing_prige_block">							
							<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>					
							<span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
						</div>
						<h3>Chontaduro Barcelona</h3>
						<span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>                               
						<span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
					  </div>
					</div>
					<div className="utf_star_rating_section" data-rating="4.5">
					  <div className="utf_counter_star_rating">(4.5)</div>
					  <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
					  <span className="like-icon"></span>
					</div>
					</a> 
				</div>

				<div className="col-md-4 col-lg-4 col-md-12 isotope-item food_drinks restaurants"> <a href="/Detail_annonces" className="utf_listing_item-container" data-marker-id="2">
					<div className="utf_listing_item"> <img src="images/utf_listing_item-02.jpg" alt="" /> <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span>
					  <div className="utf_listing_item_content"> 
						<div className="utf_listing_prige_block">							
							<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>					
						</div>
						<h3>The Lounge & Bar</h3>
						<span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>                
						<span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
					  </div>
					</div>
					<div className="utf_star_rating_section" data-rating="4.5">
					  <div className="utf_counter_star_rating">(4.5)</div>
					  <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
					  <span className="like-icon"></span>
					</div>
					</a> 
				</div>

				<div className="col-md-4 col-lg-4 col-md-12 isotope-item travels restaurants"> <a href="/Detail_annonces" className="utf_listing_item-container" data-marker-id="3">
					<div className="utf_listing_item"> <img src="images/utf_listing_item-03.jpg" alt="" /> <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span>
					  <span className="utf_open_now">Ouvert</span>
					  <div className="utf_listing_item_content"> 
						<div className="utf_listing_prige_block">							
							<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>					
							<span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
						</div>
						<h3>Westfield Sydney</h3>
						<span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>                                
						<span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
					  </div>
					</div>
					<div className="utf_star_rating_section" data-rating="4.5">
					  <div className="utf_counter_star_rating">(4.5)</div>
					  <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
					  <span className="like-icon"></span>
					</div>
					</a> 
				</div>

				<div className="col-md-4 col-lg-4 col-md-12 isotope-item events travels"> <a href="/Detail_annonces" className="utf_listing_item-container" data-marker-id="4">
					<div className="utf_listing_item"> <img src="images/utf_listing_item-04.jpg" alt="" /> <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span>
					  <div className="utf_listing_item_content"> 
						<div className="utf_listing_prige_block">							
							<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>					
						</div>
						<h3>Ruby Beauty Center</h3>
						<span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>                                
						<span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
					  </div>
					</div>
					<div className="utf_star_rating_section" data-rating="4.5">
					  <div className="utf_counter_star_rating">(4.5)</div>
					  <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
					  <span className="like-icon"></span>
					</div>
					</a> 
				</div>

				<div className="col-md-4 col-lg-4 col-md-12 isotope-item events food_drinks"> <a href="/Detail_annonces" className="utf_listing_item-container" data-marker-id="5">
					<div className="utf_listing_item"> <img src="images/utf_listing_item-05.jpg" alt="" /> <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span> <span className="featured_tag">Featured</span>
					  <div className="utf_listing_item_content"> 
						<div className="utf_listing_prige_block">							
							<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>					
							<span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
						</div>
						<h3>UK Fitness Club</h3>
						<span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>                               
						<span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
					  </div>
					</div>
					<div className="utf_star_rating_section" data-rating="4.5">
					  <div className="utf_counter_star_rating">(4.5)</div>
					  <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
					  <span className="like-icon"></span>
					</div>
					</a> 
				</div>

				<div className="col-md-4 col-lg-4 col-md-12 isotope-item food_drinks"> <a href="/Detail_annonces" className="utf_listing_item-container" data-marker-id="6">
					<div className="utf_listing_item"> <img src="images/utf_listing_item-06.jpg" alt="" /> <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span>
					  <span className="utf_closed">Closed</span>
					  <div className="utf_listing_item_content"> 
						<div className="utf_listing_prige_block">							
							<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>					
							<span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
						</div>
						<h3>Fairmont Pacific Rim</h3>
						<span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>                                
						<span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
					  </div>
					</div>
					<div className="utf_star_rating_section" data-rating="4.5">
					  <div className="utf_counter_star_rating">(4.5)</div>
					  <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
					  <span className="like-icon"></span>
					</div>
					</a> 
				</div>
				
				<div className="col-md-4 col-lg-4 col-md-12 isotope-item restaurants"> <a href="/Detail_annonces" className="utf_listing_item-container" data-marker-id="1">
					<div className="utf_listing_item"> <img src="images/utf_listing_item-01.jpg" alt="" /> <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span> <span className="featured_tag">Featured</span>
					  <span className="utf_closed">Closed</span>
					  <div className="utf_listing_item_content"> 
						<div className="utf_listing_prige_block">							
							<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>					
							<span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
						</div>
						<h3>Chontaduro Barcelona</h3>
						<span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>                               
						<span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
					  </div>
					</div>
					<div className="utf_star_rating_section" data-rating="4.5">
					  <div className="utf_counter_star_rating">(4.5)</div>
					  <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
					  <span className="like-icon"></span>
					</div>
					</a> 
				</div>

				<div className="col-md-4 col-lg-4 col-md-12 isotope-item travels food_drinks"> <a href="/Detail_annonces" className="utf_listing_item-container" data-marker-id="2">
					<div className="utf_listing_item"> <img src="images/utf_listing_item-02.jpg" alt="" /> <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span>
					  <div className="utf_listing_item_content"> 
						<div className="utf_listing_prige_block">							
							<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $25 - $55</span>					
						</div>
						<h3>The Lounge & Bar</h3>
						<span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>                
						<span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
					  </div>
					</div>
					<div className="utf_star_rating_section" data-rating="4.5">
					  <div className="utf_counter_star_rating">(4.5)</div>
					  <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
					  <span className="like-icon"></span>
					</div>
					</a> 
				</div>

				<div className="col-md-4 col-lg-4 col-md-12 isotope-item travels restaurants"> <a href="/Detail_annonces" className="utf_listing_item-container" data-marker-id="3">
					<div className="utf_listing_item"> <img src="images/utf_listing_item-03.jpg" alt="" /> <span className="tag"><i className="im im-icon-Hotel"></i> Hotels</span>
					  <span className="utf_open_now">Ouvert</span>
					  <div className="utf_listing_item_content"> 
						<div className="utf_listing_prige_block">							
							<span className="utf_meta_listing_price"><i className="fa fa-tag"></i> $45 - $70</span>					
							<span className="utp_approve_item"><i className="utf_approve_listing"></i></span>
						</div>
						<h3>Westfield Sydney</h3>
						<span><i className="sl sl-icon-location"></i> The Ritz-Carlton, Hong Kong</span>                                
						<span><i className="sl sl-icon-phone"></i> (415) 796-3633</span>
					  </div>
					</div>
					<div className="utf_star_rating_section" data-rating="4.5">
					  <div className="utf_counter_star_rating">(4.5)</div>
					  <span className="utf_view_count"><i className="fa fa-eye"></i> 822+</span>
					  <span className="like-icon"></span>
					</div>
					</a> 
				</div>
			</div>
		</div>
		<div className="clearfix"></div>
        <div className="row">
          <div className="col-md-12">
            <div className="utf_pagination_container_part margin-top-20 margin-bottom-75">
              <nav className="pagination">
                <ul>
                  <li><a href="#"><i className="sl sl-icon-arrow-left"></i></a></li>
                  <li><a href="#" className="current-page">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#"><i className="sl sl-icon-arrow-right"></i></a></li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
	</div>
	
	<section className="utf_cta_area_item utf_cta_area2_block">
		<div className="container">
			<div className="row">
				<div className="col-md-12">
					<div className="utf_subscribe_block clearfix">
						<div className="col-md-8 col-sm-7">
							<div className="section-heading">
								<h2 className="utf_sec_title_item utf_sec_title_item2">S'abonner à la Newsletter!</h2>
								<p className="utf_sec_meta">
									Inscrivez-vous pour recevoir les dernières mises à jour et informations.
								</p>
							</div>
						</div>
						<div className="col-md-4 col-sm-5">
							<div className="contact-form-action">
								<form method="post">
									<span className="la la-envelope-o"></span>
									<input className="form-control" type="email" placeholder="Entrez votre email" required="" />
									<button className="utf_theme_btn" type="submit">S'abonner</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		  </div>
		</section>
        </div>
    )
}