import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
          <Head>
          <meta name="author" content="" />
          <meta name="description" content="" />
          <meta httpEquiv="Content-Type" content="text/html;charset=UTF-8"/>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"></link>

          <link rel="shortcut icon" href="images/favicon.png" />
          <link href ='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel ='stylesheet' /> 
          
          <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"></link>
       
          <link rel="stylesheet" href="/css/stylesheet.css" />
          <link rel="stylesheet" href="/css/mmenu.css" />
          <link rel="stylesheet" href="/css/style.css" />
          <link rel="stylesheet" href="/css/colors_version/stylesheet_1.css" />
         
          <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800&display=swap&subset=latin-ext,vietnamese" rel="stylesheet" /> 
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700,800" rel="stylesheet" type="text/css" />
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
          <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
          </Head>
        <body>
          <Main>
            
          </Main>
          <NextScript>
          <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"/>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"/>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"/>
            <script src="scripts/jquery-3.4.1.min.js"/> 
            <script src="scripts/chosen.min.js"/>
            
            
            <script src="scripts/slick.min.js"/> 
            <script src="scripts/rangeslider.min.js"/> 
            <script src="scripts/bootstrap-select.min.js"/>
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"/>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"/>

            <script src="scripts/magnific-popup.min.js"/> 
            <script src="scripts/jquery-ui.min.js"/>
            <script src="scripts/mmenu.js"/> 
            <script src="scripts/tooltips.min.js"/> 
            <script src="scripts/color_switcher.js"/>
            <script src="scripts/jquery_custom.js"/>
            <script src="scripts/man.js"/>
            

          
            <script src="scripts/extensions/themepunch.tools.min.js"/> 
            <script src="scripts/extensions/themepunch.revolution.min.js"/> 
            <script src="scripts/extensions/revolution.extension.actions.min.js"/> 
            <script src="scripts/extensions/revolution.extension.carousel.min.js"/> 
            <script src="scripts/extensions/revolution.extension.kenburn.min.js"/> 
            <script src="scripts/extensions/revolution.extension.layeranimation.min.js"/> 
            <script src="scripts/extensions/revolution.extension.migration.min.js"/> 
            <script src="scripts/extensions/revolution.extension.navigation.min.js"/> 
            <script src="scripts/extensions/revolution.extension.parallax.min.js"/> 
            <script src="scripts/extensions/revolution.extension.slideanims.min.js"/> 
            <script src="scripts/extensions/revolution.extension.video.min.js"/> 


    
            
          </NextScript>
        </body>
      </Html>
    )

  }
}

export default MyDocument