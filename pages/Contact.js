import { Divider } from '@material-ui/core'
import React from 'react'

export default function Contact(){
    return(
        <div>
            <div className="utf_contact_map margin-bottom-70"> 
                <div id="utf_single_listing_map_block">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15889.947369674564!2d-4.011070530224634!3d5.342397599999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1eb7693cb25d3%3A0xdded9e104d9ef69!2sRadiodiffusion%20T%C3%A9l%C3%A9vision%20Ivoirienne%20(RTI)!5e0!3m2!1sfr!2sci!4v1616158755677!5m2!1sfr!2sci" width="100%" height="450" style={{border:"0"}} allowfullscreen="" loading="lazy"></iframe>
                    {/* <div id="utf_single_listingmap" dataLatitude="36.778259" dataLongitude="-119.417931" dataMapIcon="im im-icon-Map2"></div>       */}
                </div>   
            </div>
            <div className="clearfix"></div>
  
            <div className="container">
                <div className="row"> 
                <div className="col-md-8">
                    <section id="contact" className="margin-bottom-70">
                    <h4><i className="sl sl-icon-phone"></i> Formulaire de Contact</h4>          
                    <form id="contactform">
                        <div className="row">
                        <div className="col-md-6">  
                            <input name="name" type="text" placeholder="Nom" required />                
                        </div>
                        <div className="col-md-6">                
                            <input name="name" type="text" placeholder="Prénoms" required />                
                        </div>
                        <div className="col-md-6">                
                            <input name="email" type="email" placeholder="Email" required />                
                        </div>
                        <div className="col-md-6">
                            <input name="subject" type="text" placeholder="Sujet" required />              
                        </div>
                        <div className="col-md-12">
                            <textarea name="comments" cols="40" rows="2" id="comments" placeholder="Votre Message" required></textarea>
                        </div>
                        </div>            
                        <input type="submit" className="submit button" id="submit" value="Envoyer" />
                    </form>
                    </section>
                </div>
      
                <div className="col-md-4">
                    <div className="utf_box_widget margin-bottom-70">
                        <h3><i className="sl sl-icon-map"></i> Adresse du bureau</h3>
                        <div className="utf_sidebar_textbox">
                        <ul className="utf_contact_detail">
                            <li><strong>Adresse Téléphonique:-</strong> <span>+ 001 245 0154</span></li>
                            <li><strong>Site Web:-</strong> <span><a href="#">www.yere2baby.com</a></span></li>
                            <li><strong>E-Mail:-</strong> <span><a href="mailto:info@example.com">info@yere2baby.com</a></span></li>
                            <li><strong>Adresse:-</strong> <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</span></li>
                        </ul>
                        </div>	
                    </div>
                </div>
                </div>
            </div>
  
            <section className="utf_cta_area_item utf_cta_area2_block">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="utf_subscribe_block clearfix">
                                <div className="col-md-8 col-sm-7">
                                    <div className="section-heading">
                                        <h2 className="utf_sec_title_item utf_sec_title_item2">S'abonner à la Newsletter!</h2>
                                        <p className="utf_sec_meta">
                                            Inscrivez-vous pour recevoir les dernières mises à jour et informations.
                                        </p>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-5">
                                    <div className="contact-form-action">
                                        <form method="post">
                                            <span className="la la-envelope-o"></span>
                                            <input className="form-control" type="email" placeholder="Entrez votre email" required="" />
                                            <button className="utf_theme_btn" type="submit">S'abonner</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>  
        </div>
    )
}